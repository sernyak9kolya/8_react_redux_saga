import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types'
import Chat from './containers/Chat/Chat';
import Modal from './containers/Modal/Modal';
import ErrorBoundary from './components/ErrorBoundary';
import { openModal, setCurrentMessageId } from './containers/Modal/actions';
import { CURRENT_USER } from './config'
import './App.css';

class App extends React.Component {
  handleKeyUp(event) {
    const { messages, openModal, setCurrentMessageId } = this.props;
    if (event.code === 'ArrowUp') {
      const myLastMessage = [...messages].reverse().find(msg => msg.userId === CURRENT_USER.id);
      setCurrentMessageId(myLastMessage.id);
      openModal();
    }
  }

  componentDidMount() {
    document.addEventListener('keyup', e => this.handleKeyUp(e));
  }

  componentWillUnmount() {
    document.removeEventListener('keyup');
  }

  render() {
    return (
      <div className="app">
        <header className="header">
          <div className="header__container container">
            <h4 className="header__logo">React Chat</h4>
          </div>
        </header>
        <div className="chatWrapper">
          <ErrorBoundary>
            <Chat />
            <Modal />
          </ErrorBoundary>
        </div>
        <footer className="footer">
          <div className="footer__container container">
            <span className="footer__copyright">2020 &copy; React Chat</span>
          </div>
        </footer>
      </div>
    );
  };
}

App.propTypes = {
  messages: propTypes.arrayOf(propTypes.object),
  openModal: propTypes.func,
  setCurrentMessageId: propTypes.func
};

const mapStateToProps = state => ({
  messages: state.messages
});

const mapDispatchToProps = {
  openModal,
  setCurrentMessageId
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
