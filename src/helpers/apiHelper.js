import { messages } from '../mockData';

const useMockData = true;

const callFakeApi = () => {
  const response = messages;

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
};

const callApi = (url, method = 'GET') => {
  const options = {
    method
  };

  return useMockData
  ? callFakeApi()
  : fetch(url, options)
      .then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load')))
      .then(resource => resource)
      .catch(error => {
        throw error;
      })
};

export { callApi };
