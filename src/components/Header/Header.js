import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import './Header.css'

class Header extends React.Component {
  render() {
    const { messages } = this.props;

    const uniqUserId = new Set();
    for (let i = 0; i < messages.length; i++) {
      if (uniqUserId.has(messages[i].userId)) {
        continue;
      }
      uniqUserId.add(messages[i].userId);
    }

    const countOfParticipants = uniqUserId.size;
    const countOfMessages = messages.length;
    const lastMessageCreatedAt = messages[countOfMessages - 1].createdAt;
    const timeOfLastMessage = moment(lastMessageCreatedAt).fromNow();

    return (
      <div className="chatHeader bg-primary text-white d-flex align-items-center">
        <h4 className="chatHeader__title">Chat Title</h4>
        <div className="chatHeader__participants">
          <i className="fa fa-user"></i>
          { `${countOfParticipants} participants` }
        </div>
        <div className="chatHeader__count">
          <i className="fa fa-envelope"></i>
          { `${countOfMessages} messages` }
        </div>
        <div className="chatHeader__last">
          { `last message: ${timeOfLastMessage}` }
        </div>
      </div>
    );
  };
}

Header.propTypes = {
  messages: propTypes.arrayOf(propTypes.object)
}

export default Header;
