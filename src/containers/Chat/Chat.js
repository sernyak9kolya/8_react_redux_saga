import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { setMessages } from './actions';
import ChatView from './ChatView';
import { getMessages } from './service';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  async componentWillMount() {
    this.setState(() => ({ isLoading: true }));
    try {
      const messages = await getMessages();
      this.props.setMessages(messages);
    } catch (error) {
      throw error;
    } finally {
      this.setState(() => ({ isLoading: false }));
    }
  }

  render() {
    const { isLoading } = this.state;
    const { messages } = this.props;

    return <ChatView messages={messages} isLoading={isLoading} />;
  };
}

Chat.propTypes = {
  messages: propTypes.arrayOf(propTypes.object),
  setMessages: propTypes.func
};

const mapStateToProps = state => ({
  messages: state.messages
});


const mapDispatchToProps = {
  setMessages
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
