import { SET_MESSAGES, ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE } from './actionTypes';
const initialState = [];

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_MESSAGES: {
      const { messages } = action.payload;
      return messages;
    }

    case ADD_MESSAGE: {
      const { message } = action.payload;
      return [
        ...state,
        message
      ]
    }

    case UPDATE_MESSAGE: {
      const { message } = action.payload;
      const messageIndex = state.findIndex(msg => msg.id === message.id);

      const newMessage = {
        ...state[messageIndex],
        text: message.text,
        editedAt: new Date().toISOString()
      };

      return [
        ...state.slice(0, messageIndex),
        newMessage,
        ...state.slice(messageIndex + 1)
      ]
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      return state.filter(msg => msg.id !== id);
    }

    default:
      return state;
  }
}
