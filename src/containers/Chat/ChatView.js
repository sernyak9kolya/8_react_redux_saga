import React from 'react';
import propTypes from 'prop-types';
import Spinner from '../../components/Spinner';
import Header from '../../components/Header/Header';
import MessageList from '../../components/MessageList/MessageList';
import MessageInput from '../../components/MessageInput/MessageInput';
import './Chat.css';

class ChatView extends React.Component {
  render() {
    const { isLoading, messages } = this.props;

    const chatContentEl = (
      <div className="chat__content d-flex flex-column">
        <Header messages={messages} />
        <MessageList messages={messages} />
        <MessageInput />
      </div>
    );

    return (
      <div className="chat">
        <div className="chat__container container">
          { isLoading ? <Spinner /> : chatContentEl }
        </div>
      </div>
    );
  };
}

ChatView.propTypes = {
  isLoading: propTypes.bool,
  messages: propTypes.arrayOf(propTypes.object)
};

export default ChatView;
