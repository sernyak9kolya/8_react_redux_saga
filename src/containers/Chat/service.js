import { callApi } from '../../helpers/apiHelper';

const API_URL = 'https://api.npoint.io/b919cb46edac4c74d0a8';

export const getMessages = async () => {
  const messages = await callApi(API_URL);
  return messages;
}